- Mục đích sử dụng:
    - Bạn đang dùng Sublime Text không muốn sử dụng phần mềm ftp khác hay vào web quản lý Hosting để chọn Upload lên hosting 1 cách mất thời gian
    - Khi Upload bằng phương pháp này thì file hay thư mục bạn upload nó sẽ tự động upload đúng đường dẫn y như trên file source local mà không sợ upload sai vị trí như khi ta thường upload file lên Hosting (Phải tìm đúng thư mục cần upload thì mới up file )
    - Tóm lại là bạn lười thì hãy dùng cách này. Cách kia dùng cho những người chăm chỉ và kiên trì

- Chức Năng:
    - Upload file, thư mục từ máy tính lên Hosting
    - Download file hay thư mục từ Hosting xuống máy tính
    - Đồng bộ source từ máy tính lên hosting hoặc từ hosting xuống máy tính
    - Xem, sửa, xóa file trên hosting từ Sublime Text

- Yêu cầu
    - Cài đặt Sublime Text
    - có tài khoản ftp trên Hosting

1. Cài đặt SFTP trên Sublime Text
    - Ấn tổ hợp phím `ctrl+shift+p` trên window hoặc `cmd+shift+p` trên OS X
    - Chọn dòng `Package Control: Install Package` và gõ enter
    - Gõ `SFTP` và chọn `Sublime SFTP` và gõ Enter và chờ cài đặt

2. Thiết lập thông số 
    - `Click phải chuột` vào thư mục chứa source trong Sublime [như hình](https://wbond.net/sublime_packages/img/sftp/sidebar_menu_unconfigured.png)
    - Chọn `SFTP/FTP` [như hình](https://wbond.net/sublime_packages/img/sftp/sidebar_menu_unconfigured.png)
    - Chọn `Map to Remote ... `[như hình](https://wbond.net/sublime_packages/img/sftp/sidebar_menu_unconfigured.png)
    - Sau đó file `sftp-config.json` sẽ được tạo ra trong thư mục đó [như hình](https://wbond.net/sublime_packages/img/sftp/remote_config.png)
    - Các bạn thiết lập các thông số sau:
        - `"type": "sftp"` : kiểu ftp, có 3 loại `sftp`, `ftp` or `ftps` . Tôi hay dùng `ftps`
        - `"host": "example.com"` : thông tin hosting của bạn thường là domain hoặc ip hosting. Tôi hay dùng IP dạng `123.123.123.123` hoặc domain dạng `ftp.domain.com` (nhớ phải có phần `ftp.` trước tên miền của bạn)
        - `"user": "username"` : tài khoản ftp của bạn. Thường là tài khoản hosting của bạn , hoặc bạn tạo tài khoản ftp mới
        - `"password": "password"` : mật khẩu của tài khoản hosting của bạn , hoặc maatk khẩu của tài khoản ftp bạn tạo
        - `"remote_path": "/example/path/"` : là nơi sẽ upload file lên hosting. Bạn phải thiết lập chính xác đường dẫn` "/example/path/"` 
            - Ví dụ: tôi có cấu hình ftp của tôi trên CPanel [như hình](https://pasteboard.co/I9iaZNY.png) 
            - có nghĩa là tài khoản ftp của tôi là `mydomain` thì tôi sửa `"user": "mydomain"`
            - phần `/home/mydomain` là phần thư mục mà tài khoản ftp đó đã được thiết lập. Trong CPanel , source web thường lưu trong thư mục `public_html`. Như vậy tôi phải thiết lập như sau `"remote_path": "/public_html/"` . Tại sao phải có `/` ở đầu và ở cuối `public_html` thì các bạn tự tìm hiểu sẽ rõ
        - Thiết lập xong thì lưu file đó lại (Lưu như nào các bạn đã biết rồi)

3. Update code và tải code lên host qua SFTP của Sublime
    - Khi sửa code xong bạn lưu file code đó
    - Sau đó ấn phải chuột vào file cần upload lên host, chọn `SFTP/FTP`, Chọn `Upload File` [như hình](https://wbond.net/sublime_packages/img/sftp/sidebar_menu_configured.png) là xong.
    - Với thư mục thì bạn làm tương tự nhưng chọn `Upload Folder`